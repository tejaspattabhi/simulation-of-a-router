Simulation of a Router
=
This applicaion downloads a particular page on specifying the http(s) URL 

Make sure the server and client are in the same network or the server is accessible to the client via a valid hostname.

## Server ##

**Execute the following commands.**  

	> gcc server.c -o server -l pthread  
	> ./server  


## Client ##
**Execute the following commands.**
   
	> gcc client.c -o client
	> ./client <IP adress>  


## Execution 
```./client <server IP/Hostname>```
the ```<server IP/Hostname>``` is the name or IP address of the server where the server side code is running.

### Note: Start the server before the client.
The remaining part of the execution will be suggested by the program. 


*In case of any troubles/issues, please mail the product owner at tejas.pattabhi@gmail.com  
Thanks,  
Admin*